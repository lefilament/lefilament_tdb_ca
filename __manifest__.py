# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    'name': "Le Filament - Tableau de Bord CA",

    'summary': """Indicateurs de l'entreprise""",
    'author': "LE FILAMENT",
    'category': 'dashboard',
    'website': "http://www.le-filament.com",
    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'depends': ['crm', 'account', 'hr_expense'],
    'data': [
        'security/lefilament_dashboard_security.xml',
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/views.xml',
        'views/schedule.xml',
        'data/ir_module_category.xml'
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
}
