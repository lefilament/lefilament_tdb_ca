# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from . import lefilament_tdb
from . import res_company
from . import hr_employee
