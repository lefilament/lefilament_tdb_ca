# -*- coding: utf-8 -*-

# © 2017 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class hr_employee(models.Model):
    _name = "hr.employee"
    _inherit = "hr.employee"

    capital = fields.Float("Apport Capital Social")
