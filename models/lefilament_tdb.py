# -*- coding: utf-8 -*-

# © 2017 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import models, fields, api

from odoo.fields import Date


class LeFilamentTdb(models.Model):
    _name = "lefilament.dashboard"
    _description = "Le Filament Dashboard"
    _order = "date_tdb desc"

    name = fields.Char('Mois', compute='get_month')
    date_tdb = fields.Date('Date', required=True,
                           default=datetime.today().strftime('%Y-%m-%d'),)

    ca_mois = fields.Float('Facturé', compute="dashboard_values", store=True)

    cmd_mois = fields.Float('Commandes',
                            compute="dashboard_values",
                            store=True)

    pipe_mois = fields.Float('Pipe', compute="dashboard_values", store=True)

    treso = fields.Float('Trésorerie', compute="dashboard_values", store=True)
    variation = fields.Float('Variation',
                             compute="dashboard_values",
                             store=True)
    charges = fields.Float('Décaissé', compute="dashboard_values", store=True)
    encaisse = fields.Float('Encaissé', compute="dashboard_values", store=True)
    charges_fixes = fields.Float('Charges Fixes', default=10000)
    runway = fields.Float('Runway', compute="runway_value", )

    @api.multi
    @api.depends('date_tdb')
    def dashboard_values(self):
        for record in self:
            if record.date_tdb:
                date_tdb = datetime.strptime(record.date_tdb, '%Y-%m-%d')

                # CA
                # FACTURÉ
                self.env.cr.execute(
                    "SELECT SUM(amount_untaxed_signed) \
                    FROM account_invoice \
                    WHERE state != 'draft' \
                    AND (type = 'out_invoice' OR type = 'out_refund') \
                    AND date >= date_trunc('month', %s) \
                    AND date < date_trunc('month', %s + interval '1' month);",
                    (date_tdb, date_tdb)
                    )
                ca_mois = self.env.cr.fetchone()[0]

                # COMMANDES TOTAL
                self.env.cr.execute(
                    "SELECT SUM(amount_untaxed) \
                    FROM sale_order \
                    WHERE invoice_status = 'to invoice' \
                    AND date_order >= date_trunc('month', %s) \
                    AND date_order < date_trunc('month', %s \
                    + interval '1' month);",
                    (date_tdb, date_tdb)
                    )
                cmd_mois = self.env.cr.fetchone()[0]

                # Trésorerie
                self.env.cr.execute(
                    "SELECT SUM(amount) \
                    FROM account_bank_statement_line \
                    WHERE date < date_trunc('month', %s \
                    + interval '1' month);",
                    (date_tdb)
                    )
                treso_total = self.env.cr.fetchone()[0]
                # CHARGES
                self.env.cr.execute(
                    "SELECT SUM(amount) \
                    FROM account_bank_statement_line \
                    WHERE amount < 0 \
                    AND date >= date_trunc('month', %s) \
                    AND date < date_trunc('month', %s + interval '1' month);",
                    (date_tdb, date_tdb)
                    )
                charges = self.env.cr.fetchone()[0]
                # ENCAISSE
                self.env.cr.execute(
                    "SELECT SUM(amount) \
                    FROM account_bank_statement_line \
                    WHERE amount > 0 \
                    AND date >= date_trunc('month', %s) \
                    AND date < date_trunc('month', %s + interval '1' month);",
                    (date_tdb, date_tdb)
                    )
                encaisse = self.env.cr.fetchone()[0]

                # CHARGES FIXES
                self.env.cr.execute(
                    "SELECT charges_fixes \
                    FROM res_company"
                    )
                charges_fixes = self.env.cr.fetchone()[0]

                # PIPE
                self.env.cr.execute(
                    "SELECT SUM(planned_revenue * probability / 100) \
                    FROM crm_lead \
                    WHERE active = True"
                    )
                pipe = self.env.cr.fetchone()[0]

                if not encaisse:
                    encaisse = 0
                if not charges:
                    charges = 0

                record.ca_mois = ca_mois
                record.cmd_mois = cmd_mois
                record.treso = treso_total
                record.charges = charges * (-1.0)
                record.encaisse = encaisse
                record.variation = encaisse + charges
                record.charges_fixes = charges_fixes
                record.pipe_mois = pipe

    @api.multi
    @api.depends('charges_fixes', 'treso')
    def runway_value(self):
        for record in self:
            if record.charges_fixes:
                record.runway = record.treso / record.charges_fixes

    @api.multi
    @api.depends('date_tdb')
    def get_month(self):
        for record in self:
            months = ['Janv',
                      'Fév',
                      'Mars',
                      'Avr',
                      'Mai',
                      'Juin',
                      'Juil',
                      'Août',
                      'Sept',
                      'Oct',
                      'Nov',
                      'Dec']
            date_tdb = record.date_tdb
            month = int(date_tdb[5:7])
            year = date_tdb[2:4]
            record.name = months[month-1] + " " + year

    @api.model
    def new_data(self):
        self.create({'date_tdb': str(datetime.now())})

    @api.model
    def retrieve_datas_dashboard(self):
        # Get fiscal years
        fiscal_date = datetime(
            datetime.now().year,
            self.env.user.company_id.fiscalyear_last_month,
            self.env.user.company_id.fiscalyear_last_day
            )
        if datetime.now() > fiscal_date:
            fy = Date.to_string(fiscal_date)
            fiscal_year = "'" + Date.to_string(fiscal_date) + "'"
            fiscal_year_next = "'" + Date.to_string(
                fiscal_date + relativedelta(years=1)) + "'"
        else:
            fy = Date.to_string(fiscal_date-relativedelta(years=1))
            fiscal_year = "'" + Date.to_string(fiscal_date - relativedelta(
                years=1)) + "'"
            fiscal_year_next = "'" + Date.to_string(fiscal_date) + "'"

        report_last_year = 0.0
        get_report = self.env['report.annuel'].search([('date', '=', fy)])
        if get_report:
            get_report.ensure_one()
            report_last_year = get_report.facture - get_report.en_cours

        # Prepare values
        res = {
            'facture': 0,
            'ca': 0,
            'commandes': 0,
            'pipe': 0,
            'pipe_win': 0,
            'pipe_to_win': 0,
            'pipe_n1': 0,
            'tresorerie': 0,
            'entree': 0,
            'sortie': 0,
            'variation': 0,
            'target': 0,
            'cca': 0,
            'capital': 0,
            'date_maj': 0,
            'a_encaisser': 0,
            'a_payer': 0,
            'fiscal_year': fiscal_year,
            'fiscal_year_next': fiscal_year_next,
        }

        self._cr.execute(
            """
            SELECT
                (SELECT COUNT(*)
                    FROM account_invoice
                ) AS id,
                (SELECT SUM(amount_untaxed_signed)
                    FROM account_invoice
                    WHERE state!='draft'
                        AND (type='out_invoice' OR type='out_refund')
                        AND date > %s AND date <= %s
                ) AS facture,
                (SELECT SUM(residual_company_signed)
                    FROM account_invoice
                    WHERE state!='draft' AND type='out_invoice'
                ) AS a_encaisser,
                (SELECT SUM(residual_company_signed)
                    FROM account_invoice
                    WHERE state!='draft' AND type='in_invoice'
                ) AS a_payer,
                (SELECT SUM(planned_revenue * probability / 100)
                    FROM crm_lead
                    WHERE active=True
                        AND (date_deadline <= %s OR date_deadline is NULL)
                ) AS pipe,
                (SELECT SUM(planned_revenue * probability / 100)
                    FROM crm_lead WHERE active=True AND date_deadline > %s
                ) AS pipe_n1,
                (SELECT SUM(planned_revenue * probability / 100)
                    FROM crm_lead
                    WHERE active=True
                        AND probability = 100
                        AND (date_deadline <= %s OR date_deadline is NULL)
                ) AS pipe_win,
                (SELECT SUM(planned_revenue * probability / 100)
                    FROM crm_lead
                    WHERE active=True
                        AND probability != 100
                        AND (date_deadline < %s OR date_deadline is NULL)
                ) AS pipe_to_win,
                (SELECT date
                    FROM account_bank_statement
                    ORDER BY ID DESC LIMIT 1
                ) AS date_maj,
                (SELECT SUM(amount)
                    FROM account_bank_statement_line
                ) AS tresorerie,
                (SELECT SUM(amount)
                    FROM account_bank_statement_line
                    WHERE amount > 0 AND date > %s
                ) AS entree,
                (SELECT SUM(amount)
                    FROM account_bank_statement_line
                    WHERE amount < 0 AND date > %s
                ) AS sortie,
                (SELECT SUM(amount)
                    FROM account_bank_statement_line
                    WHERE date > %s
                ) AS variation,
                (SELECT SUM(e.total_amount)
                    FROM hr_expense_sheet es, hr_expense e
                    WHERE es.id = e.sheet_id
                        AND e.payment_mode = 'own_account'
                        AND es.state != 'done'
                ) AS cca,
                (SELECT SUM(price_subtotal - qty_invoiced * price_unit)
                    FROM sale_order_line
                    WHERE invoice_status = 'to invoice'
                ) AS commandes;
            """
            % (fiscal_year,
               fiscal_year_next,
               fiscal_year_next,
               fiscal_year_next,
               fiscal_year_next,
               fiscal_year_next,
               fiscal_year,
               fiscal_year,
               fiscal_year)
            )
        datas = self._cr.dictfetchall()

        self._cr.execute("SELECT ca_target FROM res_company;")
        ca_target = self._cr.dictfetchall()

        self._cr.execute("SELECT sum(capital) AS capital FROM hr_employee;")
        capital = self._cr.dictfetchall()

        if datas[0]['facture']:
            res['facture'] += datas[0]['facture']
            res['ca'] += datas[0]['facture'] + report_last_year
        if datas[0]['a_encaisser']:
            res['a_encaisser'] += datas[0]['a_encaisser']
        if datas[0]['a_payer']:
            res['a_payer'] += datas[0]['a_payer']
        if datas[0]['pipe']:
            res['pipe'] += datas[0]['pipe']
        if datas[0]['pipe_win']:
            res['pipe_win'] += datas[0]['pipe_win']
        if datas[0]['pipe_to_win']:
            res['pipe_to_win'] += datas[0]['pipe_to_win']
        if datas[0]['pipe_n1']:
            res['pipe_n1'] += datas[0]['pipe_n1']
        if datas[0]['tresorerie']:
            res['tresorerie'] += datas[0]['tresorerie']
        if datas[0]['date_maj']:
            res['date_maj'] = datas[0]['date_maj']
        if datas[0]['entree']:
            res['entree'] += datas[0]['entree']
        if datas[0]['sortie']:
            res['sortie'] += datas[0]['sortie']
        if datas[0]['variation']:
            res['variation'] += datas[0]['variation']
        if datas[0]['commandes']:
            res['commandes'] += datas[0]['commandes']
        if datas[0]['cca']:
            res['cca'] += datas[0]['cca']
        if ca_target[0]['ca_target']:
            res['target'] += ca_target[0]['ca_target']
        if capital[0]['capital']:
            res['capital'] += capital[0]['capital']

        return res

    @api.model
    def tresorerie(self):
        self._cr.execute(
            """
            SELECT to_char(date_trunc('month', date),'YYYY-MM') AS mois,
                SUM(CASE WHEN amount > 0 THEN amount ELSE 0 END) AS entree,
                SUM(CASE WHEN amount < 0 THEN amount ELSE 0 END) AS sortie,
                SUM(amount) AS variation
            FROM account_bank_statement_line
            GROUP BY date_trunc('month', date)
            ORDER BY date_trunc('month', date);
            """
            )
        tresorerie = self._cr.dictfetchall()

        self._cr.execute(
            """
            SELECT
                (SELECT SUM(e.total_amount) AS fonds_propres
                    FROM hr_expense_sheet es, hr_expense e
                    WHERE es.id = e.sheet_id
                        AND e.payment_mode = 'own_account'
                        AND es.state != 'done'
                ) AS cca,
                (SELECT SUM(capital) AS capital FROM hr_employee) AS capital;
            """
            )
        fonds_propres = self._cr.dictfetchall()[0]

        if not fonds_propres['cca']:
            fonds_propres['cca'] = 0

        return {'tresorerie': tresorerie, 'fonds_propres': fonds_propres}

    @api.model
    def previ_tresorerie(self):
        self._cr.execute(
            """
            SELECT to_char(date_trunc('month', date),'YYYY-MM') AS mois,
                SUM(SUM(amount)) OVER (
                    ORDER BY date_trunc('month', date)
                ) AS treso
            FROM account_bank_statement_line
            GROUP BY date_trunc('month', date)
            ORDER BY date_trunc('month', date) DESC LIMIT 6;
            """
            )
        tresorerie = self._cr.dictfetchall()

        self._cr.execute(
            """
            SELECT
                (SELECT SUM(e.total_amount) AS fonds_propres
                    FROM hr_expense_sheet es, hr_expense e
                    WHERE es.id = e.sheet_id
                        AND e.payment_mode = 'own_account'
                        AND es.state != 'done'
                ) as cca,
                (SELECT SUM(capital) AS capital FROM hr_employee) AS capital;
            """
            )
        fonds_propres = self._cr.dictfetchall()[0]

        self._cr.execute(
            """
            SELECT to_char(date_trunc('month', date_due),'YYYY-MM') AS mois,
                SUM(CASE WHEN type = 'in_invoice'
                    THEN residual_company_signed
                    ELSE 0 END
                    ) AS f_fournisseur,
                SUM(CASE WHEN type = 'out_invoice'
                    THEN residual_company_signed
                    ELSE 0 END
                    ) AS f_client
            FROM account_invoice
            WHERE state != 'draft' AND state != 'paid'
            GROUP BY date_trunc('month', date_due)
            ORDER BY date_trunc('month', date_due);
            """
            )
        factures = self._cr.dictfetchall()

        self._cr.execute(
            """
            SELECT periode,
                SUM(montant)
            FROM previ_treso
            WHERE periode = 12
            GROUP BY periode;
            """
            )
        charges_mensuelles = self._cr.dictfetchall()

        self._cr.execute(
            """
            SELECT periode,
                SUM(montant)
            FROM previ_treso
            WHERE periode = 3
            GROUP BY periode;
            """
            )
        charges_trimestre = self._cr.dictfetchall()

        self._cr.execute(
            """
            SELECT to_char(date_trunc('month', date), 'YYYY-MM') as mois,
                SUM(montant)
            FROM previ_treso
            WHERE periode = 1
            GROUP BY date_trunc('month', date);
            """
            )
        charges_fixes = self._cr.dictfetchall()

        if not fonds_propres['cca']:
            fonds_propres['cca'] = 0

        return {
            'tresorerie': tresorerie,
            'fonds_propres': fonds_propres,
            'factures': factures,
            'charges_fixes': charges_fixes,
            'charges_mensuelles': charges_mensuelles,
            'charges_trimestre': charges_trimestre,
            }
