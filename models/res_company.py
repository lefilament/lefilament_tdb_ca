# -*- coding: utf-8 -*-

# © 2017 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import models, fields, api
from odoo.fields import Date


class res_company(models.Model):
    _name = "res.company"
    _inherit = "res.company"

    ca_target = fields.Integer("Objectif Chiffre d'Affaire")
    charges_fixes = fields.Integer('Charges Fixes')
    previ_treso_ids = fields.One2many('previ.treso',
                                      'company_id',
                                      'Prévisionnel')
    report_annuel = fields.One2many('report.annuel',
                                    'company_id',
                                    'Report Annuel')



class previ_treso(models.Model):
    _name = "previ.treso"
    _description = "Previsionnel de tresorerie"
    _order = 'name'

    company_id = fields.Many2one(
        'res.company',
        'Company',
        default=lambda self: self.env.user.company_id.id)
    name = fields.Char('Nom')
    periode = fields.Selection([(12, 'Mensuel'),
                                (3, 'Trimestriel'),
                                (1, 'Annuel')], srting='Période')
    date = fields.Date('Date')
    montant = fields.Float('Montant')


class ReportsAnnuel(models.Model):
    _name = "report.annuel"
    _rec_name = 'date'
    _description = "Reports Annuels"
    _order = 'date desc'

    @api.model
    def _default_date(self):
        fiscal_date = datetime(datetime.now().year,
                               self.env.user.company_id.fiscalyear_last_month,
                               self.env.user.company_id.fiscalyear_last_day)
        if datetime.now() > fiscal_date:
            fiscal_year = Date.to_string(fiscal_date)
        else:
            fiscal_year = Date.to_string(fiscal_date-relativedelta(years=1))
        return fiscal_year

    company_id = fields.Many2one(
        'res.company',
        'Company',
        default=lambda self: self.env.user.company_id.id)
    date = fields.Date(string='Date cloture',
                       required=True,
                       default=_default_date)
    en_cours = fields.Float(string='Travaux en cours',
                            required=True,
                            default=0.0)
    facture = fields.Float(string="Produits constatés d'avance",
                           required=True,
                           default=0.0)
