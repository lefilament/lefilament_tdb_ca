 // © 2017 Le Filament (<http://www.le-filament.com>)
 // License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

odoo.define('lefilament_tdb.dashboard_year', function (require) {
	"use strict";

	var core = require('web.core');
	var formats = require('web.formats');
	var Model = require('web.Model');
	var session = require('web.session');
	var Widget = require('web.Widget');

	var QWeb = core.qweb;

	var _t = core._t;
	var _lt = core._lt;

	var YearDashboardView = Widget.extend({
	    template: 'YearDashboard',

	    events: {
	        'click #facture': function(e) {
	        	var fiscalyear = e.target.dataset.fiscalyear;
	        	this.facture(fiscalyear);
	        },
	        'click #facture_non_encaisse': function() {
	        	this.facture_non_encaisse();
	        },
	        'click #commandes': function() {
	        	this.commandes();
	        },
	        'click #pipe_link': function(e) {
	        	var fiscalyearnext = e.target.dataset.fiscalyearnext;
	        	this.pipe(fiscalyearnext);
	        },
	        'click #pipe_n1_link': function(e) {
	        	var fiscalyearnext = e.target.dataset.fiscalyearnext;
	        	this.pipe_n1(fiscalyearnext);
	        },
	        'click #fournisseur_link': function() {
	        	this.fournisseur();
	        },
	        'click #releve': function() {
	        	this.releve();
	        },
	    },

	    init: function() {
	      var result = this._super.apply(this, arguments);
	      return result;
	    },

	    willStart: function() {
	        var deferred = new jQuery.Deferred();
	        var self = this;
	        this.values = {};
	        this.progess = 0;

	        var invoice_view_id = new Model('ir.model.data').call('xmlid_to_res_id', ['account.invoice_form'])
	        	.then(function(results) {
	        		self.invoice_view_id = results;
	        	});

	        var pipe_view_id = new Model('ir.model.data').call('xmlid_to_res_id', ['crm.crm_case_form_view_oppor'])
	        	.then(function(results) {
	        		self.pipe_view_id = results;
	        	});

	        var dash_model = new Model('lefilament.dashboard');
	        dash_model.call('retrieve_datas_dashboard')
                .then(function(results) {
                    self.values = results;

                    self.pfact2 = (self.values.ca);
					self.pcomm2 = (self.values.commandes);
					self.ppipe2_win = (self.values.pipe_win);
					self.ppipe2_to_win = (self.values.pipe_to_win);

                    if (self.values.target > 0) {
                    	self.pfact = (self.values.ca / self.values.target * 100).toFixed(0);
                    	self.pcomm = (self.values.commandes / self.values.target * 100).toFixed(0);
						self.ppipe_win = (self.values.pipe_win / self.values.target * 100).toFixed(0);
						self.ppipe_to_win = (self.values.pipe_to_win / self.values.target * 100).toFixed(0);
						self.ptarg = 100-self.pfact-self.pcomm-self.ppipe_win-self.ppipe_to_win;
						self.ptarg2 = self.values.target -self.pfact2-self.pcomm2-self.ppipe2_to_win-self.ppipe2_win;
						self.total = ((self.values.ca + self.values.commandes + self.values.pipe_win) / self.values.target * 100).toFixed(0);
                    } else {
                    	self.pfact = 'n/a';
                    	self.pcomm = 'n/a';
						self.ppipe_win = 'n/a';
						self.ppipe_to_win = 'n/a';
						self.ptarg = 'n/a';
						self.ptarg2 = 'n/a';
						self.total = 'n/a';
                    }
					
					
					self.total2 = self.pfact2 + self.pcomm2 + self.ppipe2_win

					self.target = self.values.target
                    
                    deferred.resolve();
                });
	          return jQuery.when(this._super.apply(this, arguments),deferred);
	    },

	    start: function() {
	    	return this.render_chart();
	    },

	    render_chart: function(chart) {
	    	self = this;

			/////////////////////////////////////////
            //    Etat d'avancement -> Bar Chart   //
            /////////////////////////////////////////
            
            this.ctx = this.$el.find('#target')[0].getContext('2d');

            var ptarg = this.ptarg;
            var max_xaxis = 100;

            if (this.ptarg < 0 ) {
            	ptarg = 0;
            	max_xaxis = 100 - this.ptarg;
            }
	    		
   			var dataset_stacked = [
		        { label: "Chiffre d'Affaire", data: [this.pfact], backgroundColor: '#8ED8A2', },
		        { label: 'Commandes', data: [this.pcomm], backgroundColor: '#F6DCA2', },
		        { label: 'Gagné', data: [this.ppipe_win], backgroundColor: '#F6CCA2', },
		        { label: 'Pipe', data: [this.ppipe_to_win], backgroundColor: '#F6ACA2', },
		        { label: 'To Do', data: [ptarg], backgroundColor: '#eee', },
		        ];

		    var label = 'Année ' + moment(Date.now()).format('YYYY');

		    this.targetData = {
	            labels : [label],
	            datasets : dataset_stacked
	        };

            var options = { 
            	responsive: true,
            	legend: {
            		display: false,
            	},
            	layout: { 
            		padding: { left:0, right: 0, bottom: 20, top: 20, },
            	},
            	scales: {
		            xAxes: [{
		                stacked: true,
		                scaleShowLabels: false,
                        display : true ,
                        ticks: {
		                    max: max_xaxis,
		                    stepSize: 25,
		                    fontSize: 9,
		                    fontColor: '#999',
		                    callback: function(value, index, values) {
		                        return value + '%';
		                    },
		                },
		                gridLines: {
		                	zeroLineColor: 'rgba(0, 0, 0, 0.1)',
		                	drawBorder: false,
		                	tickMarkLength: 2,
		                },
		            }],
                    yAxes: [{
                            stacked: true,
                            scaleShowLabels: false,
                            display : false ,
                        }]
		        },
		        tooltips: {
		        	backgroundColor: 'rgba(255,255,255,0.8)',
		        	titleFontStyle: 'normal',
		        	titleFontColor: '#999',
		        	bodyFontColor: '#777',
		        	callbacks: {
	                    label: function(tooltipItems, data) { 
	                        return (tooltipItems.xLabel * self.target  / 100000).toLocaleString('fr', { maximumFractionDigits: 2 }) + ' K€';
	                    }
	                }
		        },
		        responsive: true,
            }

            var myLineChart = new Chart(this.ctx, { type: 'horizontalBar', data: this.targetData, options } );

	    },

	    render_monetary: function(value) {
	        value = value.toLocaleString('fr', { maximumFractionDigits: 0 }) + ' €';
	        return value;
	    },
	    render_keur: function(value) {
	        value = (value/ 1000).toLocaleString('fr', { maximumFractionDigits: 0 }) + ' K€';
	        return value;
	    },
	    render_percent: function(value) {
	        value = value.toLocaleString('fr', { maximumFractionDigits: 1 }) + ' %';
	        return value;
	    },
	    render_date: function(value) {
	    	var dateFormat = new Date(value);
            var new_value = moment(dateFormat).format('Do MMM YYYY');
            return new_value;
	    },
	    render_monetary_color: function(value) {
	        if (value >= 0)
	          value = '<span class="positive">'+value.toLocaleString('fr', { maximumFractionDigits: 0 }) + ' €</span>';
	        else
	          value = '<span class="negative">'+value.toLocaleString('fr', { maximumFractionDigits: 0 }) + ' €</span>';
	        
	        return value;
	    },

	    facture: function(fiscalyear) {
	    	var self = this;
	    	var context = { 'user_id': this.session.uid, }

        	var action = ({
            	type: 'ir.actions.act_window',
             	res_model: 'account.invoice',
            	view_type: 'form',
            	view_mode: 'tree,form',
            	views: [[false, 'list'], [false, 'pivot'], [false, 'graph'], [self.invoice_view_id, 'form']],
            	domain: [['state','in',['open','paid']],['type','=','out_invoice'],['date_invoice','>=',fiscalyear]],
            	target:'current',
            	name: 'Facturé',
            	context: context
        	})

        	console.log(action.domain);

        	this.do_action(action);
	    },
	    commandes: function() {
	    	var self = this;
	    	var context = { 'user_id': this.session.uid, }

        	var action = ({
            	type: 'ir.actions.act_window',
             	res_model: 'sale.order',
            	view_type: 'form',
            	view_mode: 'tree,form',
            	views: [[false, 'list'], [false, 'form']],
            	domain: [['invoice_status','=','to invoice']],
            	target:'current',
            	name: 'Commandes en cours',
            	context: context
        	})

        	this.do_action(action);
	    },
	    pipe: function(fiscalyearnext) {
	    	var self = this;
	    	var context = { 'user_id': this.session.uid, }

        	var action = ({
            	type: 'ir.actions.act_window',
             	res_model: 'crm.lead',
            	view_type: 'form',
            	view_mode: 'tree,form',
            	views: [[false, 'kanban'], [false, 'list'], [false, 'pivot'], [false, 'graph'], [this.pipe_view_id, 'form']],
            	domain: ['|',['date_deadline','<=', fiscalyearnext],['date_deadline','=', null], ['type','=','opportunity'] ],
            	target:'current',
            	name: 'Pipe',
            	context: context
        	})

        	this.do_action(action);
	    },
	    pipe_n1: function(fiscalyearnext) {
	    	var self = this;
	    	var context = { 'user_id': this.session.uid, }

        	var action = ({
            	type: 'ir.actions.act_window',
             	res_model: 'crm.lead',
            	view_type: 'form',
            	view_mode: 'kanban,tree,form',
            	views: [[false, 'kanban'], [false, 'list'], [false, 'pivot'], [false, 'graph'], [this.pipe_view_id, 'form']],
            	domain: [['type','=','opportunity'],['date_deadline','>', fiscalyearnext]],
            	target:'current',
            	name: 'Pipe',
            	context: context
        	})

        	this.do_action(action);
	    },
	    facture_non_encaisse: function() {
	    	var self = this;
	    	var context = { 'user_id': this.session.uid, }

        	var action = ({
            	type: 'ir.actions.act_window',
             	res_model: 'account.invoice',
            	view_type: 'form',
            	view_mode: 'tree,form',
            	views: [[false, 'list'], [false, 'pivot'], [false, 'graph'], [self.invoice_view_id, 'form']],
            	domain: [['state','=','open'],['type','=','out_invoice']],
            	target:'current',
            	name: 'Factures en cours',
            	context: context
        	})

        	this.do_action(action);
	    },
	    fournisseur: function() {
	    	var self = this;
	    	var context = { 'user_id': this.session.uid, }

        	var action = ({
            	type: 'ir.actions.act_window',
             	res_model: 'account.invoice',
            	view_type: 'form',
            	view_mode: 'tree,form',
            	views: [[false, 'list'], [false, 'form']],
            	domain: [['state','=','open'],['type','=','in_invoice']],
            	target:'current',
            	name: 'Factures fournisseurs en cours',
            	context: context
        	})

        	this.do_action(action);
	    },
	    releve: function() {
	    	var self = this;
	    	var context = { 'user_id': this.session.uid, }

        	var action = ({
            	type: 'ir.actions.act_window',
             	res_model: 'account.bank.statement',
            	view_type: 'form',
            	view_mode: 'tree,form',
            	views: [[false, 'list'], [false, 'form']],
            	// domain: [['state','=','open'],['type','=','in_invoice']],
            	target:'current',
            	name: 'Relevés en cours',
            	context: context
        	})

        	this.do_action(action);
	    },
	});

	core.action_registry.add('lefilament_tdb.dashboard_year', YearDashboardView);


});

