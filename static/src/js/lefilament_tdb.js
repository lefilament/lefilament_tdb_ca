// © 2017 Le Filament (<http://www.le-filament.com>)
// License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

odoo.define('lefilament_tdb.dashboard', function(require) {
  var core = require('web.core');
  var data = require('web.data');
  var Widget = require('web.Widget');
  var model = require('web.Model');
  var form_common = require('web.form_common');
  var session = require('web.session');
  var formats = require('web.formats');

  var _t = core._t;
  var QWeb = core.qweb;

  var LeFilamentDashboard = Widget.extend({
    template: 'Dashboard',

    events: {
      'click #export_excel': 'export_excel',
    },

    init: function() {
      var result = this._super.apply(this, arguments);
      return result;
    },

    willStart: function() {
        var deferred = new jQuery.Deferred();
        var self = this;
        this.request = {};
        new data.Query(new model("lefilament.dashboard"), [])
                .order_by("-date_tdb")
                .limit(12)  
                .all()
                .then(function(results) {
                    self.request = results;
                    deferred.resolve();
                });
          return jQuery.when(this._super.apply(this, arguments),deferred);
    },

    start: function() {

    },

    export_excel: function(e) {
      var table = TableExport(this.$el.find('#dataTable'), {
          headers: true,  
          footers: false,
          formats: ['xlsx',],
          filename: 'Tableau de Bord',               // (id, String), filename for the downloaded file, (default: 'id')
          bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: true)
          exportButtons: false,                        // (Boolean), automatically generate the built-in export buttons for each of the specified formats (default: true)
          position: 'bottom',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
          ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
          ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
          trimWhitespace: true                        // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: false)
        });

      var exportData = table.getExportData();
      console.log(exportData);
      table.export2file(exportData.dataTable.xlsx.data, exportData.dataTable.xlsx.mimeType, exportData.dataTable.xlsx.filename, exportData.dataTable.xlsx.fileExtension);
      
    },
    
    render_monetary: function(value) {
        value = value.toLocaleString('fr', { maximumFractionDigits: 0 }) + ' €';
        return value;
    },

    render_monetary_color: function(value) {
        if (value >= 0)
          value = '<td class="positive">+ '+value.toLocaleString('fr', { maximumFractionDigits: 0 }) + ' €</td>';
        else
          value = '<td class="negative">'+value.toLocaleString('fr', { maximumFractionDigits: 0 }) + ' €</td>';
        
        return value;
    },
    render_percent: function(value) {
        value = value.toLocaleString('fr', { maximumFractionDigits: 1 }) + ' %';
        return value;
    },

    render_decimal: function(value) {
        value = value.toLocaleString('fr', { maximumFractionDigits: 2 });
        return value;
    },
    render_decimal_runway: function(value) {
        if (value < 3)
          value = '<td class="negative">'+value.toLocaleString('fr', { maximumFractionDigits: 2 }) + '</td>';
        else
          value = '<td>'+value.toLocaleString('fr', { maximumFractionDigits: 2 }) + '</td>';
        
        return value;
    },

  });

    core.action_registry.add('lefilament_tdb.dashboard', LeFilamentDashboard);

});