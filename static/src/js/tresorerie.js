// © 2017 Le Filament (<http://www.le-filament.com>)
// License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

odoo.define('lefilament_tdb.tresorerie', function (require) {
	"use strict";

	var core = require('web.core');
	var formats = require('web.formats');
	var Model = require('web.Model');
	var session = require('web.session');
	var Widget = require('web.Widget');

	var QWeb = core.qweb;

	var _t = core._t;
	var _lt = core._lt;

	var TresorerieView = Widget.extend({
	    template: 'Tresorerie',

	    init: function() {
	      var result = this._super.apply(this, arguments);
	      return result;
	    },

	    willStart: function() {
	        var deferred = new jQuery.Deferred();
	        var self = this;
	        this.values = {};
	        
	        var dash_model = new Model('lefilament.dashboard');
	        dash_model.call('tresorerie')
                .then(function(results) {
                    self.values = results;
                    deferred.resolve();
                });
	          return jQuery.when(this._super.apply(this, arguments),deferred);
	    },

	    start: function() {
	    	return this.render_chart();
	    },

	    render_chart: function(chart) {
	    	self = this;

			/////////////////////////////////////////
            //      Trésorerie  -> Bar Chart       //
            /////////////////////////////////////////
            this.ctx = this.$el.find('#tresorerie')[0].getContext('2d');

            var entree = [];
            var sortie = [];
            var variation = [];
            var treso = [];
            var cca = [];
            var fonds_propres = []
            var labels = [];
            var minTab = 11;

            var tabLength = this.values.tresorerie.length - 1;

            if ( tabLength < 12 ) {
            	minTab = tabLength;
            }

            _.each(this.values.tresorerie, function(value, key) {
            	if (key == 0 ) {
            		treso.push(value.variation);
            	} else {
            		var amount = treso[key-1] + value.variation;
            		treso.push(amount);
            	}
            	
            });
            
            for (var i = (tabLength-minTab); i < tabLength+1 ; i++) {
            	entree.push(this.values.tresorerie[i].entree);
            	sortie.push(this.values.tresorerie[i].sortie * (-1) );
            	variation.push(this.values.tresorerie[i].variation);
            	cca.push(this.values.fonds_propres.cca);
            	fonds_propres.push(this.values.fonds_propres.cca+this.values.fonds_propres.capital);
            	labels.push(moment(this.values.tresorerie[i].mois).format('MMM YYYY'));
            }
            
   			var datasets = [
   				{ label: 'Trésorerie', data: treso.slice(tabLength-11, tabLength+1), backgroundColor: 'transparent',borderColor: '#5E8ED5', },
   				{ label: 'CCA', data: cca, backgroundColor: 'transparent',borderColor: '#FFA063', borderWidth: 1.5, radius: 0, },
   				{ label: 'Fonds Propres', data: fonds_propres, backgroundColor: 'transparent',borderColor: '#FCA7B3', borderWidth: 1.5, radius: 0, },
   				{ label: 'Variation', data: variation, backgroundColor: 'rgba(255, 197, 98, 0.3)',borderColor: '#FFC562', borderWidth: 1, radius: 1, },
		        { label: 'Entrées', data: entree, backgroundColor: 'rgba(81, 210, 183, 0.3)', borderColor: '#51d2b7', borderWidth: 1, radius: 1, },
		        { label: 'Sorties', data: sortie, backgroundColor: 'rgba(249, 96, 117, 0.3)', borderColor: '#F96075', borderWidth: 1, radius: 1, },
		        ];

		    var options = {
			        scales: {
			            yAxes: [{
			                ticks: {
			                    beginAtZero:true,
			                }
			            }]
			        },
			        tooltips: {
		        	backgroundColor: 'rgba(255,255,255,0.8)',
		        	titleFontStyle: 'normal',
		        	titleFontColor: '#999',
		        	bodyFontColor: '#777',
		        	callbacks: {
	                    label: function(tooltipItems, data) { 
	                        return ' ' + (tooltipItems.yLabel / 1000).toLocaleString('fr', { maximumFractionDigits: 1 }) + ' k€';
	                    }
	                }
		        },
		        responsive: true,
			    }

		    this.targetData = {
	            labels : labels,
	            datasets : datasets
	        };

            var myLineChart = new Chart(this.ctx, { type: 'line', data: this.targetData, options: options } );

	    },

	});

	core.action_registry.add('lefilament_tdb.tresorerie', TresorerieView);


});

